# Extract calibrated target data after split from MS
# split(vis=msfile,
#       field=target,
#       outputvis=splitted_ms,
#       datacolumn='corrected',
#       width=4096,
#       keepflags=False)

from numpy import recarray
import numpy as np
import pyrap.tables as pt

import os
import sys

## --- inputs --- ##
if len(sys.argv) < 2:
    quit()
ms_filename=sys.argv[1]
print(ms_filename)
DEBUG=True
## --- inputs --- ##


# Data: Complex value for each of 4 correlations per spectral channel
# IDs correspond to values RR (5), RL (6), LR (7), LL (8), XX (9), XY (10), YX (11), and YY (12)
corr_prods_id = {5: 'RR', 6: 'RL', 7: 'LR', 8: 'LL',
                 9: 'XX', 10: 'XY', 11: 'YX', 12: 'YY'}
with pt.table(ms_filename+'/POLARIZATION') as tb:
    corr_prods = tb.getcol("CORR_TYPE")[0]
    # associate corr prod with 4 tuple indices
    lin_prods = np.array([corr_prods_id[prod] for prod in corr_prods])
    Xidx = np.nonzero(lin_prods == 'XX')[0][0]  # H pol
    Yidx = np.nonzero(lin_prods == 'YY')[0][0]  # V pol
if DEBUG:
    print('Correlation products {}'.format(corr_prods))
    for prod in corr_prods:
        print('  {:2} : {}'.format(prod, corr_prods_id[prod]))
# H = XX
print('H pol {} indices in corr products'.format(Xidx))
# V = YY
print('V pol {} indices in corr products'.format(Yidx))


with pt.table(ms_filename+'/SPECTRAL_WINDOW') as tb:
    data = tb.getvarcol("NUM_CHAN")
    for spw in data.keys():
        n_chans = data[spw][0]
print('Number channels: {}'.format(n_chans))


with pt.table(ms_filename+'/ANTENNA') as tb:
    antennas = tb.getcol("NAME")
antennas = np.array(antennas)
if DEBUG:
    print('Antennas: {}'.format(antennas))
print('Number antennas: {}'.format(len(antennas)))


# find unique timestamps indices
with pt.table(ms_filename) as tb:
    # extract baselines
    time = tb.getcol("TIME_CENTROID")
    len_ = len(time)
    uniquetime = list(set(time))

    bl_ant1_idx = tb.getcol("ANTENNA1")
    bl_ant2_idx = tb.getcol("ANTENNA2")

bl_ant1 = antennas[np.array(bl_ant1_idx)]
bl_ant2 = antennas[np.array(bl_ant2_idx)]
# find indices of unique timestamps
time = np.array(time)
uniquetime = np.sort(uniquetime)
if DEBUG:
    print('Baseline indices: {}'.format(bl_ant1_idx))
    print('Baseline indices: {}'.format(bl_ant2_idx))
    print('Baseline antennas: {}'.format(bl_ant1))
    print('Baseline antennas: {}'.format(bl_ant2))
    print('{} timestamps with {} unique'.format(
        len(time), len(uniquetime)))
print('Number dumps: {}'.format(len(uniquetime)))
print('Number baselines: {}'.format(len(bl_ant1_idx)/len(uniquetime)))


# setup for output
desc = {
        'names': ('timestamp',
                  'XX',
                  'XY',
                  'YX',
                  'YY'),
        'formats': (float,
                    float,
                    float,
                    float,
                    float),
       }
visibilities = recarray((0,), dtype=desc)


epoch=3506716800.
with pt.table(ms_filename) as tb:
    for idx, ts in enumerate(uniquetime):
        # read all baselines and channels per timestamp
        tsidx = (time == ts)
        startrow = np.nonzero(tsidx)[0][0]
        nrow = np.sum(tsidx)
        print(ts, startrow, nrow, time[startrow], time[startrow+nrow-1])
        # per block dimension: nbls x nchans x ncorrs
        data = tb.getcol("DATA",
                         startrow=startrow,
                         nrow=nrow).squeeze()
        if DEBUG:
            print(idx, ts, startrow, nrow)
            if idx < len(uniquetime)-1:
                print(idx, time[startrow], time[startrow+nrow],
                      time[startrow+nrow]-time[startrow],
                      bl_ant1[startrow], bl_ant2[startrow], data.shape)
        # average over channels and baselines
        if (len(data.shape) > 2):
            [xx, xy, yx, yy] = data.mean(axis=1).mean(axis=0)
        else:
            re = np.real(data)
            im = np.imag(data)
            [xx, xy, yx, yy] = np.sqrt(re.mean(axis=0)**2 + im.mean(axis=0)**2)
        visibilities.resize(visibilities.size+1)
        visibilities[-1] = (ts - epoch,
                            xx, xy, yx, yy)
print('number of ts + visibilities', visibilities.shape)

# write to CSV file
filename = os.path.splitext(ms_filename)[0]+'.csv'
with open(filename, 'w') as fout:
    fout.write('TS, {}\n'.format(', '.join(lin_prods)))
    for line in visibilities:
        outline = '{:}, {:.10f}, {:.10f}, {:.10f}, {:.10f}'.format(
                  line['timestamp'],
                  line['XX'],
                  line['XY'],
                  line['YX'],
                  line['YY'])
        fout.write('{}\n'.format(outline))

# -fin-
