from astropy.time import Time
from numpy import recarray

import matplotlib.dates as mdates
import matplotlib.pylab as plt
import numpy as np
import sys

## --- inputs --- ##
if len(sys.argv) < 2:
    quit()
filename=sys.argv[1]
print(filename)
DEBUG=True
## --- inputs --- ##


# setup for input
desc = {
        'names': ('timestamp',
                  'XX',
                  'XY',
                  'YX',
                  'YY'),
        'formats': (float,
                    float,
                    float,
                    float,
                    float),
       }
visibilities = recarray((0,), dtype=desc)

# read input file assuming format: TS, XX, XY, YX, YY
with open(filename, 'r') as fin:
    headings = fin.readline()
    data = fin.readlines()

for cnt, line in enumerate(data):
    line_data = line.strip().split(',')
    ts = float(line_data[0])
    [xx, xy, yx, yy] = np.array(line_data[1:]).astype(float)

    visibilities.resize(visibilities.size+1)
    visibilities[-1] = (ts, xx, xy, yx, yy)

ts = Time(visibilities['timestamp'], format='unix').datetime
# stokesI = XX + YY
stokesI = visibilities['XX'] + visibilities['YY']

fig, ax = plt.subplots(nrows=2,
                       ncols=1,
                       sharex=True,
                       figsize=[15, 8],
                       facecolor='white')
plt.subplots_adjust(wspace=0, hspace=0.05)
ax[0].plot_date(ts, visibilities['XX'], 'b,')
ax[0].plot_date(ts, visibilities['YY'], 'r,')
ax[0].set_ylim(0, 0.006)
ax[0].tick_params(axis='y', labelsize=8)
ax[0].set_ylabel('XX,YY [Jy]', fontsize=10)
ax[1].plot_date(ts, stokesI, 'k.')
ax[1].tick_params(axis='y', labelsize=8)
ax[1].set_ylabel('Stokes I [Jy]', fontsize=10)
myFmt = mdates.DateFormatter("%H:%M")
ax[1].xaxis.set_major_formatter(myFmt)
fig.autofmt_xdate(rotation=45)
ax[1].tick_params(axis='x', labelsize=8)
ax[1].set_xlabel("Time (UTC) on {}".format(
    ts[0].strftime("%Y-%m-%d")),
    fontsize=10)
plt.show()

# -fin-
