# Extract calibrated auto correlated target data after split from MS
# split(vis=msfile,
#       field=target,
#       outputvis=splitted_ms,
#       datacolumn='corrected',
#       correlation='XX,YY',
#       width=4096,
#       keepflags=False)

from numpy import recarray
import numpy as np
import pyrap.tables as pt

import os
import sys

## --- inputs --- ##
if len(sys.argv) < 2:
    quit()
ms_filename=sys.argv[1]
print(ms_filename)
DEBUG=True
## --- inputs --- ##


# Data: Complex value for each of 4 correlations per spectral channel
# IDs correspond to values RR (5), RL (6), LR (7), LL (8), XX (9), XY (10), YX (11), and YY (12)
corr_prods_id = {5: 'RR', 6: 'RL', 7: 'LR', 8: 'LL',
                 9: 'XX', 10: 'XY', 11: 'YX', 12: 'YY'}
with pt.table(ms_filename+'/POLARIZATION') as tb:
    corr_prods = tb.getcol("CORR_TYPE")[0]
    # associate corr prod with 4 tuple indices
    lin_prods = np.array([corr_prods_id[prod] for prod in corr_prods])
    Xidx = np.nonzero(lin_prods == 'XX')[0][0]  # H pol
    Yidx = np.nonzero(lin_prods == 'YY')[0][0]  # V pol
if DEBUG:
    print('Correlation products {}'.format(corr_prods))
    for prod in corr_prods:
        print('  {:2} : {}'.format(prod, corr_prods_id[prod]))
# H = XX
print('H pol {} indices in corr products'.format(Xidx))
# V = YY
print('V pol {} indices in corr products'.format(Yidx))


with pt.table(ms_filename+'/SPECTRAL_WINDOW') as tb:
    data = tb.getvarcol("NUM_CHAN")
    for spw in data.keys():
        n_chans = data[spw][0]
print('Number channels: {}'.format(n_chans))


with pt.table(ms_filename+'/ANTENNA') as tb:
    antennas = tb.getcol("NAME")
antennas = np.array(antennas)
n_ants = len(antennas)
if DEBUG:
    print('Antennas: {}'.format(antennas))
print('Number antennas: {}'.format(n_ants))

# find unique timestamps indices
with pt.table(ms_filename) as tb:
    # extract baselines
    time = tb.getcol("TIME_CENTROID")

    bl_ant1_idx = tb.getcol("ANTENNA1")
    bl_ant2_idx = tb.getcol("ANTENNA2")

len_ = len(time)
uniquetime = list(set(time))
time = np.array(time)
uniquetime = np.sort(uniquetime)
bl_ant1 = antennas[np.array(bl_ant1_idx)]
bl_ant2 = antennas[np.array(bl_ant2_idx)]
baselines = list(set(np.char.add(bl_ant1, bl_ant2)))
n_bl = len(baselines)
if DEBUG:
    print('Baseline indices: {}'.format(bl_ant1_idx))
    print('Baseline indices: {}'.format(bl_ant2_idx))
    print('Baseline antennas: {}'.format(bl_ant1))
    print('Baseline antennas: {}'.format(bl_ant2))
    print('{} timestamps with {} unique'.format(
        len(time), len(uniquetime)))
print('Number dumps: {}'.format(len(uniquetime)))
print('Number baselines: {}'.format(n_bl))


# per timestamps sum over auto and cross correlation baselines
n_ts = uniquetime.size
ts0 = np.min(time)
Hauto_sum = np.zeros((n_ts, 2), dtype=float)
Vauto_sum = np.zeros((n_ts, 2), dtype=float)
with pt.table(ms_filename) as tb:
    for cnt, bl in enumerate(np.sort(baselines)):
        ant1 = bl[:4]
        ant2 = bl[4:]

        # timestamps with ant1 in baselines
        bl1_mask = (bl_ant1==ant1)
        # timestamps with ant2 in baselines
        bl2_mask = (bl_ant2==ant2)
        # find timestamps for baseline
        bl_mask = np.logical_and(bl1_mask, bl2_mask)
        bl_ts_idx = np.nonzero(bl_mask)[0]

        if DEBUG:
            print(cnt, ant1, ant2, '{}-{}'.format(
                  np.unique(bl_ant1[bl1_mask])[0],
                  np.unique(bl_ant2[bl2_mask])[0]))
            print(bl_mask.sum(),
                  bl_ts_idx,
                  uniquetime.size)

        if ant1 == ant2:
            # ant<x>h ant<x>h and ant<x>v ant<x>v
            print('Reading autocorrelations')

            for bl_ts in bl_ts_idx:
                idx = np.nonzero(uniquetime==time[bl_ts])[0][0]
                data = tb.getcol("DATA",
                                 startrow=bl_ts,
                                 nrow=1).squeeze()
                Hauto_sum[idx] += np.abs(data)

        else:
            # ant<x>h ant<y>h and ant<x>v ant<y>v
            print('Reading cross-correlations')

            for bl_ts in bl_ts_idx:
                idx = np.nonzero(uniquetime==time[bl_ts])[0][0]
                data = tb.getcol("DATA",
                                 startrow=bl_ts,
                                 nrow=1).squeeze()
                Vauto_sum[idx] += np.abs(data + data.conj())


# calculate beam output
# tab = abs(sum_bl(cross+cross.conj)) + sum_bl(auto)
tab_data = (Hauto_sum + Vauto_sum)/float(n_ants)
print(tab_data.shape)

# write to CSV file
filename = os.path.splitext(ms_filename)[0]+'.csv'
epoch=3506716800.
with open(filename, 'w') as fout:
    fout.write('TS, XX, YY\n')
    for idx, line in enumerate(tab_data):
        [xx, yy] = line
        ts = uniquetime[idx]
        print(idx, ts, xx, yy)
        outline = '{}, {:.10f}, {:.10f}'.format(
                  ts-epoch,
                  xx,
                  yy)
        fout.write('{}\n'.format(outline))

# -fin-



