from astropy.time import Time
from numpy import recarray

import matplotlib.dates as mdates
import matplotlib.pylab as plt
import numpy as np
import sys

## --- inputs --- ##
if len(sys.argv) < 2:
    quit()
filenames=sys.argv[1:]
DEBUG=True
## --- inputs --- ##


def input_(filename):
    # read input file
    with open(filename, 'r') as fin:
        headings = fin.readline()
        data = fin.readlines()
    return headings, data


def unpack_(data):
    # read input file assuming format: TS, XX, XY, YX, YY
    XX = []
    YY = []
    TS = []
    for cnt, line in enumerate(data):
        line_data = line.strip().split(',')
        TS.append(float(line_data[0]))
        [xx, _, _, yy] = np.array(line_data[1:]).astype(float)
        XX.append(float(line_data[1]))
        YY.append(float(line_data[4]))

#     ts = Time(np.array(TS), format='unix').datetime
    ts = np.array(TS)
    xx = np.array(XX)
    yy = np.array(YY)
    return ts, xx, yy


fig, ax = plt.subplots(nrows=1,
                       ncols=len(filenames),
                       sharey=True,
                       figsize=[21, 5],
                       facecolor='white')
plt.subplots_adjust(wspace=0, hspace=0)
myFmt = mdates.DateFormatter("%m/%dT%H:%M")
fig_, ax_ = plt.subplots(nrows=1,
                         ncols=len(filenames),
                         sharey=True,
                         figsize=[21, 5],
                         facecolor='white')
plt.subplots_adjust(wspace=0.1, hspace=0)
d = .015 # how big to make the diagonal lines in axes coordinates

for cnt, file_ in enumerate(np.sort(filenames)):
    print(cnt, file_)
    if len(filenames) > 1:
        ax = ax[cnt]
        ax_ = ax_[cnt]

    # hide the spines between axes
    # arguments to pass plot, just so we don't keep repeating them
    kwargs = dict(transform=ax_.transAxes,
                  color='k',
                  clip_on=False)
    if cnt < len(filenames)-1:
        print('right')
        ax.spines['right'].set_visible(True)
        ax.yaxis.tick_left()
        ax_.spines['right'].set_visible(False)
        ax_.yaxis.tick_left()
        ax_.plot((1-d,1+d), (-d,+d), **kwargs)
        ax_.plot((1-d,1+d),(1-d,1+d), **kwargs)
    else:
        print('left')
        ax.spines['left'].set_visible(True)
        ax.yaxis.tick_right()
        ax_.spines['left'].set_visible(False)
        ax_.yaxis.tick_right()
        kwargs.update(transform=ax_.transAxes)  # switch to the bottom axes
        ax_.plot((-d,+d), (1-d,1+d), **kwargs)
        ax_.plot((-d,+d), (-d,+d), **kwargs)

    _, data = input_(file_)
    [ts, xx, yy] = unpack_(data)
    ts_dt = Time(ts, format='unix').datetime
    if cnt < 1:
        ts0 = ts[0]
    ts_ts = (ts-ts0)/3600.

    stokesI = xx + yy
    ax.plot_date(ts_dt, stokesI, 'k.')
    ax_.plot(ts_ts, stokesI, 'k.')


    ax.tick_params(labelright='off')
    ax_.tick_params(labelright='off')
    ax.xaxis.set_major_formatter(myFmt)
    fig.autofmt_xdate(rotation=45)
    ax.tick_params(axis='x', labelsize=8)
    ax.set_xlabel("Time (UTC) on {}".format(
        ts_dt[0].strftime("%Y-%m-%d")),
        fontsize=10)
    ax_.set_xlabel("Time (UTC) on {}".format(
        ts_dt[0].strftime("%Y-%m-%dT%H:%M:%S")),
        fontsize=10)

if len(filenames) > 1:
    ax = ax[0]
ax.tick_params(axis='y', labelsize=8)
ax.set_ylabel('Stokes I [Jy]', fontsize=10)

fig.savefig('xcorr_timeseries.png', dpi=300)
plt.show()

# -fin-
